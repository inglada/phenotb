/*=========================================================================
  Program:   phenotb
  Language:  C++

  Copyright (c) CESBIO. All rights reserved.

  See phenotb-copyright.txt for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "otbWrapperChoiceParameter.h"

#include "phenoFunctions.h"
#include "itkBinaryFunctorImageFilter.h"

namespace otb
{
namespace Wrapper
{

class SigmoFitting : public Application
{
public:
/** Standard class typedefs. */
  typedef SigmoFitting     Self;
  typedef Application                   Superclass;

  
/** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(SigmoFitting, otb::Application);

private:
  void DoInit() override
  {
    SetName("SigmoFitting");
    SetDescription("");

    // Documentation
    SetDocName("Double sigmoid fitting");
    SetDocLongDescription("");
    SetDocLimitations("None");
    SetDocAuthors("Jordi Inglada");
    
    AddParameter(ParameterType_InputImage,  "in",   "Input Image");
    SetParameterDescription("in", "Input image");

    AddParameter(ParameterType_InputImage,  "mask",   "Mask Image");
    SetParameterDescription("in", "Input image");

    AddParameter(ParameterType_String, "dates", "Date File");
    SetParameterDescription("dates", "Name of the file containing the dates for the images");

    AddParameter(ParameterType_OutputImage, "out",  "Output Image");
    SetParameterDescription("out", "Output image");
    
    AddRAMParameter();
  }

  virtual ~SigmoFitting()
  {
  }


  void DoUpdateParameters() override
  {
    // Nothing to do here : all parameters are independent
  }


  void DoExecute() override
  {

    // prepare the vector of dates
    auto dates = pheno::parse_date_file(this->GetParameterString("dates"));
    // pipeline
    FloatVectorImageType::Pointer inputImage = this->GetParameterImage("in");
    FloatVectorImageType::Pointer maskImage = this->GetParameterImage("mask");
    using FunctorType = pheno::TwoCycleSigmoFittingFunctor<FloatVectorImageType::PixelType>;
    auto filter = itk::BinaryFunctorImageFilter<FloatVectorImageType, FloatVectorImageType, FloatVectorImageType, FunctorType>::New();
    inputImage->UpdateOutputInformation();
    maskImage->UpdateOutputInformation();
    filter->SetInput(0, inputImage);
    filter->SetInput(1, maskImage);
    filter->GetFunctor().SetDates(dates);
    filter->UpdateOutputInformation();
    // Set the output image
    SetParameterOutputImage("out", filter->GetOutput());
  }


};

}
}

OTB_APPLICATION_EXPORT(otb::Wrapper::SigmoFitting)
