# =========================================================================
# Program:   pĥenotb
# Language:  C++
#
# Copyright (c) CESBIO. All rights reserved.
#
# See pĥenotb-copyright.txt for details.
#
# This software is distributed WITHOUT ANY WARRANTY; without even
# the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
# PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

add_executable(explore explore.cxx)
target_link_libraries(explore OTBCommon OTBIO OTBPheno)

add_executable(date_parsing date_parsing.cxx)
target_link_libraries(date_parsing OTBCommon OTBIO OTBPheno)

add_executable(explore-maiseo explore-maiseo.cxx)
target_link_libraries(explore-maiseo OTBCommon OTBIO OTBPheno)

add_executable(explore-sirhyus explore-sirhyus.cxx)
target_link_libraries(explore-sirhyus OTBCommon OTBIO OTBPheno)

add_executable(explore-projections explore-projections.cxx)
target_link_libraries(explore-projections OTBCommon OTBIO OTBPheno)


add_executable(explore-lai explore-lai.cxx)
target_link_libraries(explore-lai OTBCommon OTBIO OTBPheno)

add_executable(explore-sg explore-sg.cxx)
target_link_libraries(explore-sg OTBCommon OTBIO OTBPheno)

otb_create_application(NAME           SigmoFitting
                       SOURCES        SigmoFitting.cxx
                       LINK_LIBRARIES OTBIO;OTBCommon;OTBPheno)


add_executable(test-sigmo test-sigmo.cxx)
target_link_libraries(test-sigmo OTBCommon OTBIO OTBPheno)

add_executable(sigmo-maiseo sigmo-maiseo.cxx)
target_link_libraries(sigmo-maiseo OTBCommon OTBIO OTBPheno)