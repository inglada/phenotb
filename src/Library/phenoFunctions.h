/*=========================================================================

  Program:   phenotb
  Language:  C++

  Copyright (c) Jordi Inglada. All rights reserved.

  See phenotb-copyright.txt for details.

  This software is distributed WITHOUT ANY WARRANTY; without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef _PHENOFUNCTIONS_H_
#define _PHENOFUNCTIONS_H_

#include <vnl/vnl_least_squares_function.h>
#include <vnl/algo/vnl_levenberg_marquardt.h>
#include <vnl/algo/vnl_matrix_inverse.h>
#include <vnl/vnl_transpose.h>
#include <functional>
#include <algorithm>
#include "itkVariableLengthVector.h"
#include "phenoTypes.h"
#include "dateUtils.h"



namespace pheno
{
using FunctionType = std::function<VectorType (const VectorType&, const VectorType&)>;

template<ContainerC V>
using FilterType = std::function<double (V, const V&)>;

using MinMaxType = std::pair<double, double>;

using CoefficientType = VectorType;

using ApproximationErrorType = double;

using ApproximationResultType = std::tuple<CoefficientType, MinMaxType, VectorType, VectorType,
                                           ApproximationErrorType>;

class ParameterCostFunction : public vnl_least_squares_function
{
public:
  ParameterCostFunction(unsigned int nbPars, unsigned int nbD, const VectorType& yy, const VectorType& tt, FunctionType func) :
    vnl_least_squares_function(nbPars, nbD, no_gradient), nbDates(nbD), y(yy), t(tt), phenoFunction(std::move(func)) {}

  inline
  void f(const VectorType& x, VectorType& fx) override
  {
    auto yy = phenoFunction(t, x);
    for(auto i=0; i<nbDates; ++i)
      fx[i] = (y[i] - yy[i]);
  }

private:
  VectorType y;
  VectorType t;
  unsigned int nbDates;
  FunctionType phenoFunction;
};

/** Gaussian weighting of a vector. The gaussian is centered on the maximum of the profile
    and its stdev is estimated by tresholding the vector at max*thres.
*/
double gaussianFunction(double x, double m, double s);
template <ContainerC V>
V gaussianWeighting(const V& p, const V& t, double m, double s);
template <ContainerC V>
V gaussianWeighting(const V& p, const V& t, double thres=0.6);
template <ContainerC V>
V gaussianClipping(const V& p, const V& t, double thres=0.6);

/// Filter the profile and date vectors by applying a predicate. The predicate takes as parameter the index of the vectors.
template <ContainerC V, DateContainerC W, PredicateC P>
std::pair<V, V> filter_profile(V vec, W date_vec, P pred);

/// Smooth a profile using a weighting of the type pow(fabs(t[i]-t[j]),alpha)
template <ContainerC V>
V smooth_profile(V profile, V t, unsigned int radius, double alpha);
/** Optimise the function f using a Levenberg Marquardt minimizer
    the x vector contains the initial guess and will hold the result.
    The function returns the optimisation error
*/
double optimize(VectorType& x, ParameterCostFunction f);

/// Types of fitting errors
enum class FittingErrors {OK, HasNan, NOK};

/// Filter a temporal profile y(t) with filter f and radius.
template <ContainerC V>
V profileFilter(const V& y, const V& t, typename std::function<double (V, const V&)> f, unsigned int radius);
/// Get the median of a vector
template <ContainerC V>
double getMedian(V y, const V&);
/// Get the value of the middle point of y(t) when approximated by a local parabola
template <ContainerC V>
double getLocalParabola(const V& y, const V& t);
/// Compute the 3 coefficients of a parabola which best approximates y(t)
template <ContainerC V>
V getParabola(const V& y, const V& t);

namespace normalized_sigmoid{
/**
   The normalised double sigmoid or double logistic function is:
   f(t) = \frac{1}{1+exp(\frac{x_0-t}{x_1})}-\frac{1}{1+exp(\frac{x_2-t}{x_3})}
   Where:
   x_0 (resp. x_2) is the left (resp. right) inflection point
   x_1 (resp. x_3) is the rate of change at the left (resp. right) inflection point
*/
template <ContainerC V>
V F(const V& t, const V& x);

/// Estimates the first guess for the parameters given the profile p
template <ContainerC V>
V guesstimator(V p, V t);

/// Error diagnostics
template <ContainerC V>
FittingErrors error_diagnostics(const V& x, const V& p, const V& t);

/// Performs the approximation of the normalised sigmoid of profile(t)
template <ContainerC V>
ApproximationResultType Approximation(const V& profile, const V& t);
/** Performs the approximation of the normalised sigmoid of the principal cycle of profile(t) by
    1. applying the approximation once,
    2. applying the approximation to the residuals
    3. subtracting the approximated residuals from the original profile
    4. approximating the result of the subtraction
*/
template <ContainerC V, DateContainerC W>
ApproximationResultType PrincipalCycleApproximation(const V& profile, const W& t);

/** Performs the approximation of a profile as the sum of a pricipal cycle approximation and the
     approximation of the residuals. Returns the approximated profile and the results of the 2
     approximations.
*/
template <ContainerC V, DateContainerC W>
std::tuple<VectorType, ApproximationResultType, ApproximationResultType> TwoCycleApproximation(const V& profile, const W& t);
}

/// The STICS function
namespace STICS{
VectorType F(const VectorType& t, const VectorType& x);
}

/// The double sigmoid
namespace sigmoid{
/**
   The double sigmoid or double logistic function is:
   f(t) = (\frac{1}{1+exp(\frac{x_0-t}{x_1})}-\frac{1}{1+exp(\frac{x_2-t}{x_3})})*x_4+x_5
   Where:
   x_0 (resp. x_2) is the left (resp. right) inflection point
   x_1 (resp. x_3) is the rate of change at the left (resp. right) inflection point
   x_4 is the amplitude (max-min)
   x_5 is the background (min) value
*/
template <ContainerC V>
V F(const V& t, const V& x);

/// Estimates the first guess for the parameters given the profile p
template <ContainerC V>
V guesstimator(V p, V t);

/// Estimates the first guess for the parameters given the profile p using a first Gaussian approximation
template <ContainerC V>
V guesstimator_with_gaussian(V p, V t);
}

/// Symmetric Gaussian
namespace gaussian{
/**
   The double gaussian  is:
   f(t) = (exp(-\frac{t-x0}{x_1})^2)*x_2+x_3
   Where:
   x_0 is the position of the maximum
   x_1 is the width
   x_2 is the amplitude (max-min)
   x_3 is the background (min) value
*/
VectorType F(const VectorType& t, const VectorType& x);

/// Estimates the first guess for the parameters given the profile p
VectorType guesstimator(VectorType p, VectorType t);
}

/// Convert from OTB pixel to vector
template<typename ValueType>
inline
VectorType pixelToVector(const itk::VariableLengthVector<ValueType>& p);

template <typename PixelType>
struct TwoCycleSigmoFittingFunctor
{
  struct DifferentSizes{};
  std::vector<tm> dates;
  VectorType dv;

  void SetDates(const std::vector<tm>& d) {
    dates = d;
    dv = VectorType{static_cast<unsigned int>(dates.size())};
    for(auto i=0; i<dates.size(); i++)
      {
      dv[i] = pheno::doy(dates[i]);
      }

  }
  // dates is already given in doy
  // valid pixel has a mask==0
  PixelType operator()(PixelType pix, PixelType mask)
  {
    auto nbDates = pix.GetSize();

    if(dates.size()!=nbDates) throw DifferentSizes{};

    PixelType tmpix{nbDates};
    tmpix.Fill(typename PixelType::ValueType{0});

    // If the mask says all dates are valid, keep the original value
    if(mask == tmpix) return pix;
    
    VectorType vec(nbDates);
    VectorType mv(nbDates);
    
    for(auto i=0; i<nbDates; i++)
      {
      vec[i] = pix[i];
      mv[i] = mask[i];
      }


    // A date is valid if it is not NaN and the mask value == 0.
    auto pred = [=](int e) { return !(std::isnan(vec[e])) &&
                             (mv[e]==(typename PixelType::ValueType{0})); };
    auto f_profiles = filter_profile(vec, dates, pred);

    decltype(vec) profile=f_profiles.first;
    decltype(vec) t=f_profiles.second;

    // If there are not enough valid dates, keep the original value
    if(profile.size() < 4) return pix;

    auto approx = normalized_sigmoid::TwoCycleApproximation(profile, t);
    auto x_1 = std::get<0>(std::get<1>(approx));
    auto mm1 = std::get<1>(std::get<1>(approx));
    auto x_2 = std::get<0>(std::get<2>(approx));
    auto mm2 = std::get<1>(std::get<2>(approx));

    // Compute the approximation
    auto tmpres = normalized_sigmoid::F(dv, x_1)*(mm1.second-mm1.first)+mm1.first
      + normalized_sigmoid::F(dv, x_2)*(mm2.second-mm2.first)+mm2.first;
    // The result uses either the original or the approximated value depending on the mask value
    PixelType result(nbDates);
    for(auto i=0; i<nbDates; i++)
      result[i] = ((mv[i]==(typename PixelType::ValueType{0}))?pix[i]:tmpres[i]);

    return result;
  }

  bool operator!=(const TwoCycleSigmoFittingFunctor a)
  {
    return (this->dates != a.dates) || (this->dv != a.dv) ;
  }

  bool operator==(const TwoCycleSigmoFittingFunctor a)
  {
    return !(*this == a);
  }
  
};
}
// include the definition of the template functions
#include "phenoFunctions.txx"
#endif
